#█▓▒░ Path(s)

# Homebrew’s path appears first
export PATH="/usr/local/bin:$PATH"

# NVM – Nodejs Version Manager
source $HOME/.nvm/nvm.sh

# RVM – Ruby Version Manager
source $HOME/.rvm/scripts/rvm
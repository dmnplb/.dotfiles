#                 ██
#                ░██
#  ██████  ██████░██
# ░░░░██  ██░░░░ ░██████
#    ██  ░░█████ ░██░░░██
#   ██    ░░░░░██░██  ░██
#  ██████ ██████ ░██  ░██
# ░░░░░░ ░░░░░░  ░░   ░░


#█▓▒░ Configuration

# Path
ZSH=$HOME/.oh-my-zsh

# Theme
ZSH_THEME="sorin"

#█▓▒░ Aliases

# Tools
alias subl='/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl'

# Shortcuts
alias ll='ls -al'
alias up="cd ../"

# Edit Config files
alias zshconfig="subl $HOME/.zshrc"
alias editgit="subl $HOME/.gitconfig"
alias editgitignore="subl $HOME/.gitignore_global"

# Directories
alias dotfiles="cd $HOME/.dotfiles"
alias projects="cd $HOME/Dropbox/Projects/ && ls -d -- */"

# Git
alias log='git log'
alias diff='git diff'
alias branch='git branch'
alias st='git status'
alias fetch='git fetch'
alias push='git push origin head'
alias pull='git pull'
alias fp='fetch && pull'

alias llog="git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

# Gulp/Site Commands
alias develop='gulp serve'
alias build='gulp build'
alias deploy='gulp deploy'
alias jb='jekyll build'

# Utilities
alias ss="python -m SimpleHTTPServer && open http://localhost:8000"

#█▓▒░ Plugins

plugins=(git osx)

#█▓▒░ Functions

# Create a new file in the curr. directory and then open it
new () {
  sudo touch $1 && subl $1
}

#█▓▒░ Path(s)

# Homebrew’s path appears first
export PATH="/usr/local/bin:$PATH"

# NVM – Nodejs Version Manager
source $HOME/.nvm/nvm.sh

# RVM – Ruby Version Manager
source $HOME/.rvm/scripts/rvm

# Oh-My-Zsh
source $ZSH/oh-my-zsh.sh